package ar.edu.itba.paw.model;

/**
 * Created by tritoon on 18/09/16.
 */
public enum UPDATE_STATE {
    UPDATE,CREATE,REMOVE,NEW_MAP, HIDE, SHOW
}
