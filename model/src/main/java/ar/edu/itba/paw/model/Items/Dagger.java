package ar.edu.itba.paw.model.Items;

import ar.edu.itba.paw.model.Attackable;
import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.Updatable;

import java.awt.*;
import java.util.List;

/**
 * Created by tritoon on 13/08/16.
 */
public class Dagger extends Item {

    @Override
    public void initializeSprite() {
        this.tile_names = new String[1][];
        this.tile_names[0] = new String[1];
        this.tile_names[0][0] = "/resources/img/Items/dagger.png";
        this.name = "Daga";
    }

    int calculateDamage(Player player) {
        return player.getAgility() / 10;
    }

    @Override
    public void use(Player player) {
        System.out.println("Trident used");
       player.addControlUpdate("{\"type\":\"attack\", \"attackMode\":\"true\"}");
        player.waitingAttackDirection = true;
    }

    @Override
    public boolean attack(int move, Player player, List<Updatable> enemies, List<Player> players) {
        if (move == 10) {
           player.addControlUpdate("{\"type\":\"attack\", \"attackMode\":\"false\"}");
            return false;
        }
        return attackEnemy(player,move,enemies);
    }

}
