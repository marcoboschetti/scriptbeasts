package ar.edu.itba.paw.model;

/**
 * Created by tritoon on 03/10/16.
 */
public class UpdateNewMap extends Update {

    private String content;

    public UpdateNewMap(String s) {
        super(UPDATE_STATE.NEW_MAP, null);
        content = s;
    }

    @Override
    public String toString() {
        return "{\"NEW_WORLD\":" + content + "}";
    }
}
