package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Items.Bow;

import java.util.LinkedList;

/**
 * Created by Marco on 8/3/2016.
 */
public class Hunter extends Player {

    public static final int role_id = 302;

    public Hunter(String name) {
        super(name, 40, 60, 60, 50, 3, 100, 5);
        setSprites("Hunter");
        this.setRoleId(role_id);

        this.inventory = new LinkedList<>();
        inventory.add(this.equipedWeapon = new Bow());
    }
}
