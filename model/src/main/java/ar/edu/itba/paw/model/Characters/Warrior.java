package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Items.LongSword;

import java.util.LinkedList;

/**
 * Created by Marco on 8/3/2016.
 */
public class Warrior extends Player {

    public static final int role_id = 306;

    public Warrior(String name) {
        super(name, 40, 70, 50, 40, 3, 110, 4);
        setSprites("Warrior");
        this.setRoleId(role_id);

        this.inventory = new LinkedList<>();
        inventory.add(this.equipedWeapon = new LongSword());
    }

}
