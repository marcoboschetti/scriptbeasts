package ar.edu.itba.paw.model.NPC;


import ar.edu.itba.paw.model.Attackable;
import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.Mappable;
import ar.edu.itba.paw.model.Updatable;

import java.awt.*;
import java.util.List;

public class Dog extends Attackable {

    private static final int DOG_MOVES = 4;
    private static final int DOG_MAXHEALTH = 25;


    private int movesLeft = DOG_MOVES;

    public Dog(int x, int y) {
        super(x, y);

        this.setHealth(DOG_MAXHEALTH);
        this.setMaxHealth(DOG_MAXHEALTH);

        this.tile_names = new String[4][4];
        for (int i = 0; i < 4; i++) {
            this.tile_names[i] = new String[4];
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[0][i] = "resources/img/Characters/DogU" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[1][i] = "resources/img/Characters/DogR" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[2][i] = "resources/img/Characters/DogD" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[3][i] = "resources/img/Characters/DogL" + (i + 1) + ".png";
        }

    }

    @Override
    protected void die() {
        System.out.println("Oops, dog dead");
    }

    @Override
    public boolean update(Mappable[][] world, List<Updatable> enemies, List<Player> players) {
        boolean ended = false;

        Point nextMove = Updatable.getRandomMove(getX(), getY());
        boolean isValid = Updatable.isValidMove(getId(), (int) nextMove.getX(), (int) nextMove.getY(), world, enemies, players);
        if (isValid) {
            this.setX((int) nextMove.getX());
            this.setY((int) nextMove.getY());
        }

        movesLeft--;

        if (movesLeft <= 0) {
            movesLeft = DOG_MOVES;
            ended = true;
        }


        return ended;
    }
}
