package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.WorldTiles.MapTile;

/**
 * Created by tritoon on 18/09/16.
 */
public class Update {

    private UPDATE_STATE state;
    private Mappable mappable;

    public Update(UPDATE_STATE state, Mappable mappable) {
        this.state = state;
        this.mappable = mappable;
    }

    public UPDATE_STATE getState() {
        return state;
    }

    public Mappable getMappable() {
        return mappable;
    }


    public String toString() {
        String segment = null;
        Mappable m = mappable;
        if (state == UPDATE_STATE.UPDATE) {
            if (m != null) {
                if (m instanceof Attackable && ((Attackable) m).getMaxHealth() > ((Attackable) m).getHealth()) {
                    segment = "{\"id\":\"" + m.getId() + "\", \"x\":\"" + m.getX() + "\",\"y\":\"" + m.getY() + "\",\"health\":\"" + ((Attackable) m).getHealth() + "\",\"maxHealth\":\"" + ((Attackable) m).getMaxHealth() + "\"}";
                } else if (m instanceof MapTile) {
                    segment = "{\"id\":\"" + m.getId() + "\", \"x\":\"" + m.getX() + "\",\"y\":\"" + m.getY() + "\",\"status\":\"" + ((MapTile) m).getStatus() + "\"}";
                } else {
                    segment = "{\"id\":\"" + m.getId() + "\", \"x\":\"" + m.getX() + "\",\"y\":\"" + m.getY() + "\"}";
                }
            }
        } else if (state == UPDATE_STATE.CREATE) {
            if (m != null) {
                segment = m.toString();
            }
        } else {
            if (m != null) {
                segment = "{\"id\":\"" + m.getId() + "\"}";
            }
        }
        return String.format("{\"state\":\"%s\",\"segment\":%s}",state,segment);
    }

    public void setState(UPDATE_STATE state) {
        this.state = state;
    }
}
