package ar.edu.itba.paw.model.WorldTiles;


public class Wall extends MapTile {
    public Wall(int x, int y) {
        super(x, y);
        this.tile_names = new String[1][];
        this.tile_names[0] = new String[1];
        this.tile_names[0][0] = "resources/img/wall.jpg";
    }
}
