package ar.edu.itba.paw.model.WorldTiles;

import ar.edu.itba.paw.model.FOG_STATUS;
import ar.edu.itba.paw.model.Mappable;
import ar.edu.itba.paw.model.UpdateMappable;
import ar.edu.itba.paw.model.UpdateWorldTile;

/**
 * Created by Marco on 7/26/2016.
 */
public class MapTile extends Mappable {

    private FOG_STATUS status;


    public MapTile(int x, int y, FOG_STATUS status) {
        super(x, y);
        this.status = status;
    }

    public MapTile(int x, int y) {
        super(x, y);
        this.status = FOG_STATUS.BLACK;
    }

    public FOG_STATUS getStatus() {
        return status;
    }

    public void setStatus(FOG_STATUS status) {
        this.status = status;
    }

    @Override
    public Mappable clone(){
        return new UpdateWorldTile(getX(),getY(),this.getId(),status);
    }
}
