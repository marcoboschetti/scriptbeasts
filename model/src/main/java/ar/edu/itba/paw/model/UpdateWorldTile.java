package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.WorldTiles.MapTile;

/**
 * Created by tritoon on 16/09/16.
 */
public class UpdateWorldTile extends MapTile{

    public UpdateWorldTile(int x, int y, int id, FOG_STATUS status) {
        super(x, y);
        this.setX(x);
        this.setY(y);
        this.setId(id);
        this.setStatus(status);
    }

}
