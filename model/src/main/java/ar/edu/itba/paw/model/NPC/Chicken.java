package ar.edu.itba.paw.model.NPC;


import ar.edu.itba.paw.model.Attackable;
import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.Mappable;
import ar.edu.itba.paw.model.Updatable;

import java.awt.*;
import java.util.List;

public class Chicken extends Attackable {

    private static final int CHICKEN_ATTACK = 15;
    private static final int CHICKEN_MOVES = 3;
    private static final int CHICKEN_MAXHEALTH = 15;


    private int movesLeft = CHICKEN_MOVES;

    public Chicken(int x, int y) {
        super(x, y);

        this.setHealth(CHICKEN_MAXHEALTH);
        this.setMaxHealth(CHICKEN_MAXHEALTH);

        this.tile_names = new String[4][4];
        for (int i = 0; i < 4; i++) {
            this.tile_names[i] = new String[4];
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[0][i] = "resources/img/Characters/ChickenU" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[1][i] = "resources/img/Characters/ChickenR" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[2][i] = "resources/img/Characters/ChickenD" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[3][i] = "resources/img/Characters/ChickenL" + (i + 1) + ".png";
        }

    }

    @Override
    protected void die() {
        System.out.println("Ugh, chicken dead");
    }

    @Override
    public boolean update(Mappable[][] world, List<Updatable> enemies, List<Player> players) {
        boolean ended = false;
        boolean attackedPlayer = false;

        for(int i = 0; i < players.size() && !attackedPlayer; i++){
            Player currentPlayer = players.get(i);
            if(Math.abs(currentPlayer.getX()-getX())<2 && Math.abs(currentPlayer.getY()-getY())<2){
                boolean dead = currentPlayer.removeHealth(CHICKEN_ATTACK);
                if (dead) {
                    addPendingRemoval(currentPlayer);
                }else{
                    addPendingUpdates(currentPlayer);
                }
                attackedPlayer = true;
            }
        }


        if (!attackedPlayer) {
            Point nextMove = Updatable.getRandomMove(getX(), getY());
            boolean isValid = Updatable.isValidMove(getId(),(int) nextMove.getX(), (int) nextMove.getY(), world, enemies, players);
            if (isValid) {
                this.setX((int) nextMove.getX());
                this.setY((int) nextMove.getY());
            }
        }

        movesLeft--;

        if (movesLeft <= 0) {
            movesLeft = CHICKEN_MOVES;
            ended = true;
        }

        return ended;
    }
}
