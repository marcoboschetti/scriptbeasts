package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Attackable;
import ar.edu.itba.paw.model.Items.Item;
import ar.edu.itba.paw.model.Mappable;
import ar.edu.itba.paw.model.Updatable;
import ar.edu.itba.paw.model.WorldTiles.Wall;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class Player extends Attackable {

    private String name;
    private int inteligence, force, agility, dexterity, totalMoves, view;
    private int movesLeft;
    private int roleId = -1;
    protected List<Item> inventory;
    protected Item equipedWeapon;
    private Queue<String> pendingControlUpdate;

    public boolean waitingAttackDirection = false;

    public Player(String name, int inteligence, int force, int agility, int dexterity, int totalMoves, int maxHealth, int view) {
        super(0, 0);
        this.inteligence = inteligence;
        this.force = force;
        this.agility = agility;
        this.dexterity = dexterity;
        this.totalMoves = totalMoves;
        this.view = view;
        this.setHealth(maxHealth);
        this.setMaxHealth(maxHealth);

        this.inventory = new LinkedList<>();
        this.pendingControlUpdate = new ConcurrentLinkedQueue<>();

        movesLeft = totalMoves;
        this.name = name;

        this.tile_names = new String[4][4];
    }

    protected void setSprites(String prefix) {
        for (int i = 0; i < 4; i++) {
            this.tile_names[i] = new String[4];
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[0][i] = "resources/img/Characters/" + prefix + "U" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[1][i] = "resources/img/Characters/" + prefix + "R" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[2][i] = "resources/img/Characters/" + prefix + "D" + (i + 1) + ".png";
        }
        for (int i = 0; i < 4; i++) {
            this.tile_names[3][i] = "resources/img/Characters/" + prefix + "L" + (i + 1) + ".png";
        }
    }

    public String getName() {
        return name;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        String stringified = "{" +
                "\"name\":\"" + name +
                "\", \"role_id\":\"" + this.roleId +
                "\", \"x\":\"" + this.getX() +
                "\", \"y\":\"" + this.getY() +
                "\", \"id\":\"" + this.getId() +
                "\", \"equiped_weapon_id\":\"" + this.equipedWeapon.getId() +
                "\", \"image_prefix\":[";
        for (int i = 0; i < 4; i++) {
            stringified += "[";
            for (int j = 0; j < 4; j++) {
                stringified += "\"" + this.tile_names[i][j] + "\"";
                if (j != 3) {
                    stringified += ",";
                }
            }
            stringified += "]";
            if (i != 3) {
                stringified += ",";
            }
        }

        stringified += "],\"stats\":[" + inteligence +
                "," + force +
                "," + agility +
                "," + dexterity +
                "," + totalMoves +
                "," + getMaxHealth() +
                "," + view +
                "], " + getInventoryString() + "}";
        return stringified;
    }

    public boolean processInput(int move, Mappable[][] world, List<Updatable> enemies, List<Player> players) {
        boolean ended = false;
        int x = getX();
        int y = getY();
        if (waitingAttackDirection) {
            boolean decreaseMove = equipedWeapon.attack(move, this, enemies, players);
            if (decreaseMove) {
                movesLeft--;
            }
        } else {
            int movex = 0, movey = 0;
            boolean attemptMove = false;
            if (move < 10) {
                Point nextMove = Updatable.directionToPoint(x, y, move);
                movex = nextMove.x;
                movey = nextMove.y;
                attemptMove = true;
            } else if (move == 10) {
                equipedWeapon.use(this);
            }

            if (attemptMove) {
                if (movex > 0 && movex < world.length && movey > 0 && movey < world[0].length && !(world[movex][movey] instanceof Wall)) {
                    boolean colidesEnemy = false;
                    for (int i = 0; i < enemies.size() && !colidesEnemy; i++) {
                        if (enemies.get(i).getX() == movex && enemies.get(i).getY() == movey) {
                            colidesEnemy = true;
                        }
                    }
                    if (!colidesEnemy) {
                        this.setX(movex);
                        this.setY(movey);
                        movesLeft--;
                    }
                }
            }
        }

        if (movesLeft <= 0) {
            ended = true;
            movesLeft = totalMoves;
            addControlUpdate("{\"type\":\"attack\", \"attackMode\":\"false\"}");
            waitingAttackDirection = false;
            //TODO: Implement a neutral-control state, when no input is expected

            //TODO: Add a real regen mechanism
            this.setHealth(Math.min(getMaxHealth(),this.getHealth() + 5));


            //curlife,force,agility, desxtrexity,speed,maxlife
            addControlUpdate("{\"type\":\"stats\", \"field\":\"curlife\", \"value\":\""+getHealth()+"\"}");
        }

        return ended;
    }

    public String getInventoryString() {

        String ans = "\"inventory\":[";
        for (int i = 0; i < inventory.size(); i++) {
            Item item = inventory.get(i);
            ans += item.toString();
            if (i < inventory.size() - 1) {
                ans += ",";
            }
        }
        ans += "]";
        return ans;

    }

    public void addControlUpdate(String s) {
        pendingControlUpdate.offer(s);
    }

    public Queue<String> getAllControlUpdates() {
        return pendingControlUpdate;
    }

    public void resetAllControlUpdates() {
        pendingControlUpdate.clear();
    }

    public int getInteligence() {
        return inteligence;
    }

    public int getForce() {
        return force;
    }

    public int getAgility() {
        return agility;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getView() {
        return view;
    }

    public int getMovesLeft() {
        return movesLeft;
    }


    @Override
    protected void die() {
        System.out.println(name + " is now dead =/");
    }

    @Override
    public boolean update(Mappable[][] world, List<Updatable> enemies, List<Player> players) {
        throw new IllegalStateException("Actually, players should not be updated...");
    }

    public boolean removeHealth(int health) {
        boolean ans = super.removeHealth(health);
        addControlUpdate("{\"type\":\"stats\", \"field\":\"curlife\", \"value\":\""+getHealth()+"\"}");
        if (ans) {
            System.out.println("really, died");
        }
        return ans;
    }

    public int getTotalMoves() {
        return totalMoves;
    }
}

