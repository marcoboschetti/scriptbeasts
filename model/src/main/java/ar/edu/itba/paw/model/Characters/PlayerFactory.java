package ar.edu.itba.paw.model.Characters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco on 8/3/2016.
 */
public abstract class PlayerFactory {

    private static List<Player> roles;

    public static Player getPlayer(String name, int id){
        switch (id){
            case Alchymist.role_id:
                return new Alchymist(name);
            case Explorer.role_id:
                return new Explorer(name);
            case Hunter.role_id:
                return new Hunter(name);
            case Invoker.role_id:
                return new Invoker(name);
            case Ninja.role_id:
                return new Ninja(name);
            case Trapmaster.role_id:
                return new Trapmaster(name);
            case Warrior.role_id:
                return new Warrior(name);
            case Wizard.role_id:
                return new Wizard(name);
        }
        return null;
    }

    public static String getRoles(){
        if(roles == null){
            populateRoles();
        }
        String ans = "{\"roles\":[";
        for(int i = 0; i < roles.size(); i++){
            Player p = roles.get(i);
            ans += p;
            if(i < roles.size()-1){
                ans += ",";
            }
        }
        ans +="]}";
        System.out.println(ans);
        return ans;
    }

    private static void populateRoles() {
        roles = new ArrayList<>();
        roles.add(new Alchymist("Alquimista"));
        roles.add(new Explorer("Explorador"));
        roles.add(new Hunter("Cazador"));
        roles.add(new Invoker("Invocador"));
        roles.add(new Ninja("Ninja"));
        roles.add(new Trapmaster("Trapmaster"));
        roles.add(new Warrior("Guerrero"));
        roles.add(new Wizard("Hechicero"));
    }
}
