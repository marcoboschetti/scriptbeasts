package ar.edu.itba.paw.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tritoon on 16/09/16.
 */
public class CircleGrid {

    private static final int R = 3;
    private static Map<Integer,boolean[][]> map = new HashMap<>();

    public static boolean[][] getCircleMatrix(int radius){
        if (map.containsKey(radius)) {
            return map.get(radius);
        }

        boolean[][] newCircle = drawCircle(radius, true);
        map.put(radius,newCircle);

        return newCircle;
    }


    private static boolean[][] drawCircle(final int radius, boolean filled) {
        int centerY = radius;
        int centerX = radius;

        int d = (5 - R * 4) / 4;
        int x = 0;
        int y = radius;

        boolean[][] matrix = new boolean[2 * radius + 1][2 * radius + 1];

        do {
            matrix[centerX + x][centerY + y] = true;
            matrix[centerX + x][centerY - y] = true;
            matrix[centerX - x][centerY + y] = true;
            matrix[centerX - x][centerY - y] = true;
            matrix[centerX + y][centerY + x] = true;
            matrix[centerX + y][centerY - x] = true;
            matrix[centerX - y][centerY + x] = true;
            matrix[centerX - y][centerY - x] = true;
            if (d < 0) {
                d += 2 * x + 1;
            } else {
                d += 2 * (x - y) + 1;
                y--;
            }
            x++;
        } while (x <= y);

        if (filled) {
            for (int fy = 0; fy < radius * 2; fy++) {
                boolean foundFirst = false;
                for (int fx = 0; fx < radius * 2; fx++) {
                    if (foundFirst) {
                        if (!matrix[fx][fy]) {
                            matrix[fx][fy] = true;
                        } else {
                            foundFirst = false;
                        }
                    } else {
                        if (matrix[fx][fy]) {
                            foundFirst = true;
                        }
                    }
                }
            }
        }
        return matrix;
    }

}
