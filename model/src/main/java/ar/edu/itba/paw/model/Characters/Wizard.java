package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Items.Ceter;

import java.util.LinkedList;

/**
 * Created by Marco on 8/3/2016.
 */
public class Wizard extends Player {

    public static final int role_id = 307;

    public Wizard(String name) {
        super(name, 80, 40, 40, 60, 3, 100, 4);
        setSprites("Wizard");
        this.setRoleId(role_id);

        this.inventory = new LinkedList<>();
        inventory.add(this.equipedWeapon = new Ceter());
    }
}
