package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.WorldTiles.MapTile;
import ar.edu.itba.paw.model.WorldTiles.Path;

import java.util.*;

/**
 * Created by tritoon on 16/09/16.
 */
public class World {

    private MapTile[][] world;
    private List<MapTile> whiteTiles;
    private Set<Integer> removedCauseVisibility;

    public World(int worldWidth, int worldHeight) {
        this.removedCauseVisibility = new HashSet<>();
        this.whiteTiles = new LinkedList<>();
        world = new MapTile[worldWidth][worldHeight];

        for (int i = 0; i < (worldWidth); i++) {
            world[i] = new MapTile[(worldHeight)];
        }
    }


    public Set<Mappable> calculateWarFog(List<Player> players) {
        Map<Integer,Mappable> map = new HashMap<>();

        if(this.whiteTiles == null){
            this.whiteTiles = new LinkedList<>();
        }else {

            for (MapTile m : this.whiteTiles) {
                if (m == null) {
                    continue;
                }
                m.setStatus(FOG_STATUS.GRAY);
                map.put(m.getId(), m.clone());
            }

            whiteTiles.clear();
        }

        for (Player player : players) {
            boolean[][] circle = CircleGrid.getCircleMatrix(player.getView());
            for (int x = (-1) * player.getView(); x <= player.getView(); x++) {
                for (int y = (-1) * player.getView(); y <= player.getView(); y++) {
                    if (player.getX() + x < world.length && player.getX() - x >= 0 &&
                            player.getX() - x < world.length && player.getX() + x >= 0 &&
                            player.getY() + y < world[0].length && player.getY() - y >= 0 &&
                            player.getY() - y < world[0].length && player.getY() + y >= 0 &&
                            circle[player.getView() + x][player.getView() + y]) {
                        world[player.getX() + x][player.getY() + y].setStatus(FOG_STATUS.WHITE);
                        map.put(world[player.getX() + x][player.getY() + y].getId(),
                                world[player.getX() + x][player.getY() + y].clone());
                        this.whiteTiles.add(world[player.getX() + x][player.getY() + y]);
                    }
                }
            }
        }

        return new HashSet<>(map.values());
    }


    public void line(int x, int y, int x2, int y2, int color) {
        int[][] matrix = new int[x2 + 2][y2 + 2];

        int w = x2 - x;
        int h = y2 - y;
        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
        if (w < 0) dx1 = -1;
        else if (w > 0) dx1 = 1;
        if (h < 0) dy1 = -1;
        else if (h > 0) dy1 = 1;
        if (w < 0) dx2 = -1;
        else if (w > 0) dx2 = 1;
        int longest = Math.abs(w);
        int shortest = Math.abs(h);
        if (!(longest > shortest)) {
            longest = Math.abs(h);
            shortest = Math.abs(w);
            if (h < 0) dy2 = -1;
            else if (h > 0) dy2 = 1;
            dx2 = 0;
        }
        int numerator = longest >> 1;
        for (int i = 0; i <= longest; i++) {
            matrix[x][y] = 1;
            numerator += shortest;
            if (!(numerator < longest)) {
                numerator -= longest;
                x += dx1;
                y += dy1;
            } else {
                x += dx2;
                y += dy2;
            }
        }

        for (int a = 0; a < matrix.length; a++) {
            for (int b = 0; b < matrix[0].length; b++) {
                System.out.print(matrix[a][b]);
            }
            System.out.println();
        }

    }

    public String toJson() {
        String wholeMap = "[";
        for (int i = 0; i < world.length; i++) {
            wholeMap += "[";
            for (int j = 0; j < world[0].length; j++) {
                wholeMap += "{\"id\":\"" + world[i][j].getId() + "\",\"sprite\":\"" + world[i][j].getTile_names()[0][0] + "\"}";
                if (j != world[0].length - 1) {
                    wholeMap += ",";
                }
            }
            wholeMap += "]";
            if (i != world.length - 1) {
                wholeMap += ",";
            }
        }
        wholeMap += "]";

        return wholeMap;
    }

    public MapTile[][] getMatrix() {
        return world;
    }

    public boolean isVisible(Mappable m){
        if(m instanceof MapTile){
            return true;
        }
        if(m == null || whiteTiles.isEmpty()){
            return false;
        }

        for(MapTile t : whiteTiles){
            if(t!=null && t.getX() == m.getX() && t.getY() == m.getY()){
                return true;
            }
        }

        return false;
    }

    public void removeCauseVisibility(int id) {
        removedCauseVisibility.add(id);
    }

    public void addedCauseVisibility(int id) {
        removedCauseVisibility.remove(id);
    }

    public boolean wasRemovedCauseVisibility(int id) {
       return removedCauseVisibility.contains(id);
    }

    public void reset() {
        this.removedCauseVisibility = new HashSet<>();
        this.whiteTiles = new LinkedList<>();
        world = new MapTile[world.length][world[0].length];

        for (int i = 0; i < (world.length); i++) {
            world[i] = new MapTile[(world[0].length)];
        }
    }
}

