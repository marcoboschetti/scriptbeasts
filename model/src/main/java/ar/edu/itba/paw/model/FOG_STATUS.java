package ar.edu.itba.paw.model;

/**
 * Created by tritoon on 16/09/16.
 */
public enum FOG_STATUS {
    BLACK, GRAY, WHITE
}
