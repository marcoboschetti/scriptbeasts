package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Items.LongSword;

import java.util.LinkedList;

/**
 * Created by Marco on 8/3/2016.
 */
public class Explorer extends Player {

    public static final int role_id = 301;

    public Explorer(String name) {
        super(name, 30, 60, 60, 50, 4, 110, 6);
        setSprites("Explorer");
        this.setRoleId(role_id);

        this.inventory = new LinkedList<>();
        inventory.add(this.equipedWeapon = new LongSword());
    }
}
