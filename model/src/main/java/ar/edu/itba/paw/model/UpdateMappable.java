package ar.edu.itba.paw.model;

/**
 * Created by Marco on 8/1/2016.
 */
public class UpdateMappable extends Mappable{

    public UpdateMappable(int x, int y, int id) {
        super(x, y);
        this.setX(x);
        this.setY(y);
        this.setId(id);
    }
}
