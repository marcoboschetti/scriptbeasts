package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.Characters.Player;

import java.util.List;

/**
 * Created by Marco on 8/1/2016.
 */
public class UpdateAttackable extends Attackable{
    public UpdateAttackable(int x, int y, int id, int health, int maxHealth, String[][] tile_names) {
        super(x, y);
        this.setX(x);
        this.setY(y);
        this.setId(id);
        this.setHealth(health);
        this.maxHealth = maxHealth;
        this.tile_names = tile_names;
    }


    @Override
    protected void die() {
    }

    @Override
    public boolean update(Mappable[][] world, List<Updatable> enemies, List<Player> players) {
        throw new IllegalStateException("UpdateAttackable is a mocked clone and should not be updated");
    }
}
