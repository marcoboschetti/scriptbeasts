package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Items.PoisonPotion;
import ar.edu.itba.paw.model.Items.Trident;

import java.util.LinkedList;

/**
 * Created by Marco on 8/3/2016.
 */
public class Alchymist extends Player {

    public static final int role_id = 300;

    public Alchymist(String name) {
        super(name, 70, 40, 40, 70, 3, 100, 5);
        setSprites("Alchymist");
        this.setRoleId(role_id);

        this.inventory = new LinkedList<>();
        inventory.add(this.equipedWeapon = new Trident());
        inventory.add(new PoisonPotion());
    }

}

