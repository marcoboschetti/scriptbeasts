package ar.edu.itba.paw.model;

/**
 * A class for every Updatable with health to extend of =)
 * Created by tritoon on 02/09/16.
 */
public abstract class Attackable extends Updatable {

    /*package local*/ int health;
    /*package local*/ int maxHealth;


    public Attackable(int x, int y) {
        super(x, y);
    }

    public int getHealth() {
        return health;
    }

    // Returns if the instance is dead or not.
    public boolean setHealth(int health) {
        this.health = health;
        if (this.health <= 0) {
            this.die();
            return true;
        }
        return false;
    }

    // Returns if the instance is dead or not.
    public boolean removeHealth(int health) {
        this.health -= health;
        if (this.health <= 0) {
            this.die();
            return true;
        }
        return false;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    protected abstract void die();

    @Override
    public Mappable clone() {
        return new UpdateAttackable(getX(), getY(), getId(), health, maxHealth, this.tile_names);
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }
}
