package ar.edu.itba.paw.model;


import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.WorldTiles.Wall;

import java.awt.Point;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Marco on 7/28/2016.
 */
public abstract class Updatable extends Mappable {

    private Queue<Updatable> pendingInstanceRemoval;
    private Queue<Updatable> pendingInstanceUpdate;
    private Queue<Updatable> pendingInstanceCreation;

    public Updatable(int x, int y) {
        super(x, y);
        this.pendingInstanceRemoval = new ConcurrentLinkedQueue<>();
        this.pendingInstanceCreation = new ConcurrentLinkedQueue<>();
        this.pendingInstanceUpdate = new ConcurrentLinkedQueue<>();
    }

    public abstract boolean update(Mappable[][] world, List<Updatable> enemies, List<Player> players);

    @Override
    public String toString() {
        String stringified = "{" +
                "\"x\":\"" + this.getX() +
                "\", \"y\":\"" + this.getY() +
                "\", \"id\":\"" + this.getId() +
                "\", \"image_prefix\":[";

        for (int i = 0; i < tile_names.length; i++) {
            stringified += "[";
            for (int j = 0; j < tile_names[i].length; j++) {
                stringified += "\"" + this.tile_names[i][j] + "\"";
                if (j != 3) {
                    stringified += ",";
                }
            }
            stringified += "]";
            if (i != 3) {
                stringified += ",";
            }
        }
        stringified += "]}";
        return stringified;
    }


    public static Point directionToPoint(int x, int y, int move) {
        int movex = x, movey = y;
        switch (move) {
            case 0:
                break;
            case 1:
                movey = y - 1;
                break;
            case 2:
                movex = x + 1;
                break;
            case 3:
                movey = y + 1;
                break;
            case 4:
                movex = x - 1;
                break;
            case 5:
                movex = x + 1;
                movey = y - 1;
                break;
            case 6:
                movex = x + 1;
                movey = y + 1;
                break;
            case 7:
                movex = x - 1;
                movey = y + 1;
                break;
            case 8:
                movex = x - 1;
                movey = y - 1;
                break;
        }

        return new Point(movex, movey);
    }

    public static Point getRandomMove(int x, int y) {
        int movex = x;
        int movey = y;
        double rand = Math.random();
        if (rand < 0.25) {
            movey = y - 1;
        } else if (rand < 0.5) {
            movex = x + 1;
        } else if (rand < 0.75) {
            movey = y + 1;
        } else {
            movex = x - 1;
        }
        return new Point(movex, movey);
    }

    public static Point getScapeMove(int id, int x, int y, int scapeX, int scapeY,
                                     Mappable[][] world, List<Updatable> enemies, List<Player> players) {
        int maxX = x - 1;
        int maxY = y - 1;
        double maxDistance = Math.pow(maxX - scapeX, 2) + Math.pow(maxY - scapeY, 2);
        double currentDistance;
        if (isValidMove(id, x - 1, y + 1, world, enemies, players)
                && (currentDistance = Math.pow(x - 1 - scapeX, 2) + Math.pow(y + 1 - scapeY, 2)) > maxDistance) {
            maxDistance = currentDistance;
            maxX = x - 1;
            maxY = y + 1;
        }
        if (isValidMove(id, x + 1, y + 1, world, enemies, players)
                && (currentDistance = Math.pow(x + 1 - scapeX, 2) + Math.pow(y + 1 - scapeY, 2)) > maxDistance) {
            maxDistance = currentDistance;
            maxX = x + 1;
            maxY = y + 1;
        }
        if (isValidMove(id, x + 1, y - 1, world, enemies, players)
                && Math.pow(x + 1 - scapeX, 2) + Math.pow(y - 1 - scapeY, 2) > maxDistance) {
            maxX = x + 1;
            maxY = y - 1;
        }
        return new Point(maxX, maxY);
    }

    public static boolean isValidMove(int id, int movex, int movey, Mappable[][] world, List<Updatable> enemies, List<Player> players) {
        if (movex > 0 && movex < world.length && movey > 0 && movey < world[0].length &&
                !(world[movex][movey] instanceof Wall)) {
            boolean collidesEnemy = false;
            for (int i = 0; i < enemies.size() && !collidesEnemy; i++) {
                if (enemies.get(i).getId() != id && enemies.get(i).getX() == movex
                        && enemies.get(i).getY() == movey) {
                    collidesEnemy = true;
                }
            }
            if (!collidesEnemy) {
                boolean collidesPlayer = false;
                for (int i = 0; i < players.size() && !collidesPlayer; i++) {
                    if (players.get(i).getX() == movex && players.get(i).getY() == movey) {
                        collidesPlayer = true;
                    }
                }
                return !collidesPlayer;
            }
        }

        return false;
    }

    public void addPendingCreation(Updatable s) {
        pendingInstanceCreation.offer(s);
    }

    public Queue<Updatable> getAllPendingCreations() {
        return pendingInstanceCreation;
    }

    public void resetPendingCreations() {
        pendingInstanceCreation.clear();
    }

    public void addPendingRemoval(Updatable s) {
        pendingInstanceRemoval.offer(s);
    }

    public Queue<Updatable> getAllPendingRemoval() {
        return pendingInstanceRemoval;
    }

    public void resetPendingUpdates() {
        pendingInstanceUpdate.clear();
    }

    public void addPendingUpdates(Updatable s) {
        pendingInstanceUpdate.offer(s);
    }

    public Queue<Updatable> getAllPendingUpdates() {
        return pendingInstanceUpdate;
    }

    public void resetPendingRemoval() {
        pendingInstanceRemoval.clear();
    }

}
