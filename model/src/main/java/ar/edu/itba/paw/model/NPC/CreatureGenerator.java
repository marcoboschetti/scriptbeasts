package ar.edu.itba.paw.model.NPC;

import ar.edu.itba.paw.model.Attackable;
import ar.edu.itba.paw.model.NPC.Chicken;
import ar.edu.itba.paw.model.NPC.Dog;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Marco on 7/28/2016.
 */
public class CreatureGenerator {

    public static List<Attackable> getCreatures(int level, int roomSize) {
        int cant = 1;
        List<Attackable> creatures = new LinkedList<>();

        if(Math.random() < 0.4) {
            if (Math.random() > 0.4) {
                cant++;
            }
            for (int i = 0; i < Math.min(cant, roomSize); i++) {
                creatures.add(new Chicken(0, 0));
            }
        }else{
            creatures.add(new Dog(0,0));
        }
        return creatures;

    }
}
