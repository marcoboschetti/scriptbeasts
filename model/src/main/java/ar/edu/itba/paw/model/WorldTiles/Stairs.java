package ar.edu.itba.paw.model.WorldTiles;

/**
 * Created by Marco on 7/26/2016.
 */
public class Stairs extends MapTile {
    public Stairs(int x, int y) {
        super(x, y);
        this.tile_names = new String[1][1];
        this.tile_names[0] = new String[1];
        this.tile_names[0][0] = "resources/img/stairs.png";
    }
}
