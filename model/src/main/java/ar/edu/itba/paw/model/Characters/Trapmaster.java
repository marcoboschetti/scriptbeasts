package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Items.Maze;

import java.util.LinkedList;

/**
 * Created by Marco on 8/3/2016.
 */
public class Trapmaster extends Player {

    public static final int role_id = 305;

    public Trapmaster(String name) {
        super(name, 70, 50, 40, 60, 3, 100, 4);
        setSprites("Trapmaster");
        this.setRoleId(role_id);

        this.inventory = new LinkedList<>();
        inventory.add(this.equipedWeapon = new Maze());
    }
}
