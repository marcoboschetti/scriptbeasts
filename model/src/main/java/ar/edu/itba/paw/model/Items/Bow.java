package ar.edu.itba.paw.model.Items;

import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.Updatable;

import java.util.List;

/**
 * Created by tritoon on 13/08/16.
 */
public class Bow extends Item{

    @Override
    public void initializeSprite() {
        this.tile_names = new String[1][];
        this.tile_names[0] = new String[1];
        this.tile_names[0][0] = "/resources/img/Items/bow.png";
        this.name = "Arco";
    }

    @Override
    public void use(Player player) {
        System.out.println("Bow use called");
    }

    @Override
    public boolean attack(int move, Player player, List<Updatable> enemies, List<Player> players) {
       player.addControlUpdate("{\"type\":\"attack\", \"attackMode\":\"false\"}");

        player.waitingAttackDirection = false;
        return true;
    }

    @Override
    int calculateDamage(Player player) {
        throw new IllegalStateException("Bow cant be used to cause damage yet =/");
    }
}
