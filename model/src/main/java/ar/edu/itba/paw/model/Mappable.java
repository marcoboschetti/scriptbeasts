package ar.edu.itba.paw.model;


public abstract class Mappable {

    private static int id_count=1;

    private int x;
    private int y;
    private int id;
    protected String[][] tile_names;

    public Mappable(int x, int y){
        this.x = x;
        this.y = y;
        this.id = id_count;
        id_count++;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getId() {
        return id;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public String[][] getTile_names() {
        return tile_names;
    }

    @Override
    public Mappable clone(){
        return new UpdateMappable(x,y,id);
    }
}
