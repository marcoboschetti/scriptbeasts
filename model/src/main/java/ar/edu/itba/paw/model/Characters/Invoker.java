package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Items.Ceter;

import java.util.LinkedList;

/**
 * Created by Marco on 8/3/2016.
 */
public class Invoker extends Player {

    public static final int role_id = 303;

    public Invoker(String name) {
        super(name, 70, 50, 50, 50, 3, 100, 4);
        setSprites("Invoker");
        this.setRoleId(role_id);

        this.inventory = new LinkedList<>();
        inventory.add(this.equipedWeapon = new Ceter());
    }
}
