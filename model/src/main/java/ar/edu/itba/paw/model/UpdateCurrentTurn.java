package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.Mappable;
import ar.edu.itba.paw.model.UPDATE_STATE;
import ar.edu.itba.paw.model.Update;

/**
 * Created by tritoon on 03/10/16.
 */
public class UpdateCurrentTurn extends Update {

    private String content;

    public UpdateCurrentTurn(UPDATE_STATE state, Mappable mappable) {
        super(state, mappable);
    }

    public UpdateCurrentTurn(UPDATE_STATE state, String name, int totalMoves, int remaningMoves) {
        super(state, null);
        this.content = String.format("%s - Moves:(%d/%d)",name,remaningMoves,totalMoves);
    }

    @Override
    public String toString() {
        return "{\"currentMove\":\"" + content + "\"}";
    }
}
