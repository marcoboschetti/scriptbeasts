package ar.edu.itba.paw.model.Characters;

import ar.edu.itba.paw.model.Items.Dagger;

import java.util.LinkedList;

/**
 * Created by Marco on 8/3/2016.
 */
public class Ninja extends Player {

    public static final int role_id = 304;

    public Ninja(String name) {
        super(name, 50, 50, 70, 40, 4, 100, 5);
        setSprites("Ninja");
        this.setRoleId(role_id);

        this.inventory = new LinkedList<>();
        inventory.add(this.equipedWeapon = new Dagger());
    }

}
