package ar.edu.itba.paw.model.Items;

import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.Updatable;

import java.util.List;

/**
 * Created by tritoon on 13/08/16.
 */
public class LongSword extends Item {

    @Override
    public void initializeSprite() {
        this.tile_names = new String[1][];
        this.tile_names[0] = new String[1];
        this.tile_names[0][0] = "/resources/img/Items/longsword.png";
        this.name = "Espada Larga";
    }

    int calculateDamage(Player player) {
        return player.getDexterity() / 10;
    }

    @Override
    public void use(Player player) {
        System.out.println("Long sword use called");
       player.addControlUpdate("{\"type\":\"attack\", \"attackMode\":\"true\"}");
        player.waitingAttackDirection = true;
    }

    @Override
    public boolean attack(int move, Player player, List<Updatable> enemies, List<Player> players) {
        if (move == 10) {
           player.addControlUpdate("{\"type\":\"attack\", \"attackMode\":\"false\"}");
            return false;
        }
        return attackEnemy(player, move, enemies);
    }

}
