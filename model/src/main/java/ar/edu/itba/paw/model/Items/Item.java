package ar.edu.itba.paw.model.Items;

import ar.edu.itba.paw.model.Attackable;
import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.Mappable;
import ar.edu.itba.paw.model.Updatable;

import java.awt.*;
import java.util.List;

/**
 * Created by tritoon on 13/08/16.
 */
public abstract class Item extends Mappable{

    protected double inteligence,force,agility,dexterity;
    protected String name;

    public Item() {
        super(0,0);
        initializeSprite();
    }

    public abstract void initializeSprite();

    public abstract void use(Player player);

    @Override
    public String toString() {
        String stringified = "{" +
                "\"name\":\"" + name +
                "\", \"id\":\"" + this.getId() +
                "\", \"image_prefix\":[";

        for(int i=0;i<this.tile_names.length;i++){
            stringified += "[";
            for(int j=0;j<tile_names[i].length;j++){
                stringified += "\"" + this.tile_names[i][j]+ "\"";
                if(j < tile_names[i].length-1){
                    stringified += ",";
                }
            }
            stringified +="]";
            if(i < tile_names.length-1 ){
                stringified+= ",";
            }
        }

        stringified += "],\"stats\":[" + inteligence +
                "," + force +
                "," + agility +
                "," + dexterity +
                "]}";
        return stringified;
    }

    public abstract boolean attack(int move, Player player, List<Updatable> enemies, List<Player> players);

    protected boolean attackEnemy(Player player, int move, List<Updatable> enemies) {

        Point attackDirection = Updatable.directionToPoint(player.getX(), player.getY(), move);
        for (Updatable m : enemies) {
            if (m instanceof Attackable && m.getX() == attackDirection.getX() && m.getY() == attackDirection.getY()) {
                boolean dead = ((Attackable) m).removeHealth(this.calculateDamage(player));
                if (dead) {
                    player.addPendingRemoval(m);
                } else {
                    player.addPendingUpdates(m);
                }
            }
        }
        return true;
    }

     abstract int calculateDamage(Player player);
}
