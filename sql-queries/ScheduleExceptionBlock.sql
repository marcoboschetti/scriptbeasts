CREATE TABLE IF NOT EXISTS scheduleExceptionBlock(
	id SERIAL PRIMARY KEY,
	startDate DATE NOT NULL,
	endDate DATE NOT NULL,
	CONSTRAINT day_ck CHECK (startDate <= endDate AND startDate >= (CURRENT_DATE + INTERVAL '2 week'))
);

INSERT INTO scheduleException VALUES
(1,1),(1,2),(1,3),(1,4),(1,5);

