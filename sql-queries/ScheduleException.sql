CREATE TABLE IF NOT EXISTS scheduleException(
	complexId INTEGER NOT NULL REFERENCES complex(id),
	blockId INTEGER NOT NULL REFERENCES scheduleExceptionBlock(id)
);

INSERT INTO scheduleException VALUES
(1,1),(1,2),(1,3),(1,4),(1,5),
(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(2,9),(2,10);


