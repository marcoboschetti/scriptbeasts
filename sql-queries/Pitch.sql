CREATE TABLE IF NOT EXISTS pitch(
	id INTEGER,
	capacity INTEGER NOT NULL,
	floor INTEGER NOT NULL,
	complexId INTEGER NOT NULL REFERENCES complex(id),
  PRIMARY KEY (id, complexId)
);

/*
INSERT INTO pitch VALUES
(1,5,1,1),(2,5,2,1),(1,11,1,2),(2,5,1,2),(1,7,3,3),(2,9,2,3);
*/