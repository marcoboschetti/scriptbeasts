WITH RECURSIVE hours AS
(
  WITH range AS (
      SELECT
        ? AS start,
        ? AS xend
  )

  SELECT
    start,
    xend
  FROM range
  UNION ALL
  SELECT
    start + 1,
    xend
  FROM hours
  WHERE start < xend
)
SELECT DISTINCT ON (hours.start, capacity, price, floor)
  hours.start  AS start,
  pitch.id     AS pitchid,
  price,
  floor,
  capacity     AS size,
  schedule.day AS day,
  complexId
FROM schedule, pitch, pitchschedule, hours
WHERE schedule.id = blockid AND pitchid = pitch.id
      AND hours.start >= schedule.start AND hours.start < schedule.xend AND complexId = % d
      AND NOT EXISTS
              (SELECT
               FROM reserve
               WHERE reserve.day = '%s' AND reserve.start = hours.start AND
                     pitch.id = reserve.pitchid) %s
ORDER BY hours.start, capacity, price, floor ASC
