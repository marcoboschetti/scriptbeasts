CREATE TABLE IF NOT EXISTS complex(
	id SERIAL PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	email VARCHAR(100) NOT NULL,
	address VARCHAR(100) NOT NULL,
	stateId INTEGER REFERENCES states(id),
	phone VARCHAR(100) NOT NULL,
	UNIQUE(name)
);
/*
INSERT INTO complex VALUES
(DEFAULT,'Dummy','paco@itba.edu.ar','Calle false 123',6,1),(DEFAULT,'Tito','tito@itba.edu.ar','Calle false 123',9,2),(DEFAULT,'Jorge','jorge@itba.edu.ar','Calle false 123',4,3);

alter table complex drop column state_id;
alter table complex RENAME COLUMN stateid TO state_id;
*/