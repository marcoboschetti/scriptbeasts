CREATE TABLE IF NOT EXISTS admin(
	email VARCHAR(100) NOT NULL REFERENCES users(email),
	complexId INTEGER NOT NULL REFERENCES complex(id),
	PRIMARY KEY(email,complexId)
);
/*
INSERT INTO admin VALUES
('gmogni@itba.edu.ar',1),('gmogni@itba.edu.ar',2)*/