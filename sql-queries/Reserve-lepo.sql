WITH complexwrapper AS
(

  WITH RECURSIVE hours AS
  (
    WITH range AS (
        SELECT
          ? AS start,
          ? AS xend
    )

    SELECT
      start,
      xend
    FROM range
    UNION ALL
    SELECT
      start + 1,
      xend
    FROM hours
    WHERE start < xend
  )
  SELECT DISTINCT ON (complex.id)
    hours.start  AS start,
    pitch.id     AS pitchid,
    price,
    complexId AS complex_id,
    address,
    phone,
    floor,
    capacity     AS size,
    complex.name AS complexname,
    email AS complex_email,
    states.name  AS statesname,
    states.id    AS stateid,
    schedule.day AS day
  FROM schedule, pitch, pitchschedule, hours, states, complex
  WHERE schedule.id = blockid AND pitchid = pitch.id
        AND hours.start >= schedule.start AND hours.start < schedule.xend AND complexId = complex.id
        AND states.id = complex.stateid AND NOT EXISTS
                                                (SELECT
                                                 FROM reserve
                                                 WHERE reserve.day = '%s' AND reserve.start = hours.start AND
                                                       pitch.id = reserve.pitchid) %s
  ORDER BY complex.id, price, hours.start ASC
)
SELECT *, avg(valoration) AS val_prom
FROM complexwrapper LEFT JOIN valorations
WHERE complex_id = complexid
GROUP BY complex_id
ORDER BY price, val_prom;