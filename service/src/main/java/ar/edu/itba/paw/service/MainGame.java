package ar.edu.itba.paw.service;

import ar.edu.itba.paw.model.*;
import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.Characters.PlayerFactory;
import ar.edu.itba.paw.model.WorldTiles.Stairs;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MainGame {

    private static final int TILESIDE = 32;

    private Deque<Player> pendingPlayers;
    private List<Player> players;
    private List<Updatable> enemies;
    private Map<String, Integer> pendingInputMove;
    private Map<String, Queue<String>> pendingControlUpdate;

    private World world;
    private boolean started = false;
    private boolean playerMoves = true;
    private int currentPlayerTurn = 0;

    private ConcurrentLinkedDeque<Update> entityUpdate;

    private static MainGame instance = new MainGame();

    public static MainGame getInstance() {
        return instance;
    }

    private MainGame() {
        pendingPlayers = new ConcurrentLinkedDeque<>();
        players = new ArrayList<>();
        pendingInputMove = new HashMap<>();
        pendingControlUpdate = new HashMap<>();
        entityUpdate = new ConcurrentLinkedDeque<>();
        enemies = new ArrayList<>();
    }

    public void addPlayer(String name, int role) {
        Player newPlayer = PlayerFactory.getPlayer(name, role);
        pendingPlayers.offer(newPlayer);
        players.add(newPlayer);
        System.out.println("Player " + name + " added");
    }

    public String getNewPlayer() {
        if (!pendingPlayers.isEmpty()) {
            Player pending = pendingPlayers.poll();
            String ans = pending.toString();
            System.out.println(ans);
            return ans;
        } else {
            return "";
        }
    }

    public void informMove(String ign, int move) {
        pendingInputMove.put(ign, move);
        System.out.println("Moved:" + ign + "," + move);
        if (started && playerMoves && players.get(currentPlayerTurn).getName().equals(ign)) {
            Player currentMover = players.get(currentPlayerTurn);
            boolean ended = currentMover.processInput(move, world.getMatrix(), enemies, players);
            entityUpdate.offer(new Update(UPDATE_STATE.UPDATE, currentMover.clone()));

            for (String s : currentMover.getAllControlUpdates()) {
                pendingControlUpdate.get(ign).offer(s);
            }

            Set<Mappable> warFog = world.calculateWarFog(players);
            for (Mappable m : warFog) {
                entityUpdate.offer(new Update(UPDATE_STATE.UPDATE, m));
            }

            entityUpdate.add(new UpdateCurrentTurn(UPDATE_STATE.UPDATE, currentMover.getName(), currentMover.getTotalMoves(), currentMover.getMovesLeft()));

            updateVisibilties();
            checkStairs();

            if (ended) {
                currentPlayerTurn++;
                if (currentPlayerTurn == players.size()) {
                    currentPlayerTurn = 0;
                    playerMoves = false;
                    for (Updatable u : enemies) {
                        boolean moveEnded = false;
                        while (!(moveEnded)) {
                            moveEnded = u.update(world.getMatrix(), enemies, players);
                            entityUpdate.offer(new Update(UPDATE_STATE.UPDATE, u.clone()));
                            addPendings(u);
                        }
                    }
                    playerMoves = true;
                    pendingInputMove.clear();
                }
            }
            currentMover = players.get(currentPlayerTurn);
            entityUpdate.add(new UpdateCurrentTurn(UPDATE_STATE.UPDATE, currentMover.getName(), currentMover.getTotalMoves(), currentMover.getMovesLeft()));
            for(Player player: players) {
                addPendings(player);
            }
        }
    }

    private void checkStairs() {
        for(Player p:players){
            if(!(world.getMatrix()[p.getX()][p.getY()] instanceof Stairs)){
                return;
            }
        }
        newLevel();
        if(started) {
            entityUpdate.add(new UpdateNewMap(worldToJson()));
        }
    }

    private void addPendings(Updatable u) {
        for (Updatable m : u.getAllPendingUpdates()) {
            entityUpdate.offer(new Update(UPDATE_STATE.UPDATE, m));
        }
        for (Updatable m : u.getAllPendingCreations()) {
            entityUpdate.offer(new Update(UPDATE_STATE.CREATE, m.clone()));
            enemies.add(m);
        }
        for (Mappable m : u.getAllPendingRemoval()) {
            entityUpdate.offer(new Update(UPDATE_STATE.REMOVE, m.clone()));
            enemies.remove(m);
        }

        u.resetPendingCreations();
        u.resetPendingRemoval();
        u.resetPendingUpdates();
    }

    public int requestInput(String ign) {
        Integer ans = -1;
        if (pendingInputMove.containsKey(ign)) {
            ans = pendingInputMove.get(ign);
            if (ans == null) {
                ans = -1;
            } else {
                pendingInputMove.put(ign, null);
            }
        }
        return ans;
    }

    public String startGame(Integer width, Integer height, Integer player_cont) {

        players = players.subList(players.size() - player_cont, players.size());

        world = new World(width / TILESIDE, height / TILESIDE);

        for (Player p : players) {
            pendingControlUpdate.put(p.getName(), new ConcurrentLinkedQueue<>());
        }
        String ans = newLevel();
        started = true;

        return ans;
    }


    public String newLevel(){
        pendingInputMove.clear();
        pendingControlUpdate.clear();
        entityUpdate.clear();

        enemies.clear();

        pendingInputMove.clear();

        enemies.clear();

        world.reset();

        DungeonGenerator.generateWorld(world, players, enemies);

        for(Updatable u : enemies){
            world.removeCauseVisibility(u.getId());
        }

        updateVisibilties();

        return worldToJson();
    }

    private String worldToJson() {

        String wholeMap = world.toJson();

        String wholePlayers = "[";
        for (int i = 0; i < players.size(); i++) {
            wholePlayers += players.get(i);
            if (i != players.size() - 1) {
                wholePlayers += " , ";
            }
        }
        wholePlayers += "]";

        String wholeEnemies = "[";
        for (int i = 0; i < enemies.size(); i++) {
            wholeEnemies += enemies.get(i);
            if (i != enemies.size() - 1) {
                wholeEnemies += " , ";
            }
        }
        wholeEnemies += "]";

        String ans = "{\"tiles\":" + wholeMap + ",\"players\":" + wholePlayers + ",\"enemies\":" + wholeEnemies + "}";
        return ans;

    }


    private void updateVisibilties() {
        for (Update m : entityUpdate) {
            if (m.getMappable() != null) {
                if (m.getState() == UPDATE_STATE.UPDATE) {
                    if (!world.isVisible(m.getMappable())) {
                        entityUpdate.add(new Update(UPDATE_STATE.HIDE,m.getMappable().clone()));
                        world.removeCauseVisibility(m.getMappable().getId());
                    } else if (world.wasRemovedCauseVisibility(m.getMappable().getId())) {
                        entityUpdate.add(new Update(UPDATE_STATE.SHOW,m.getMappable().clone()));
                        world.addedCauseVisibility(m.getMappable().getId());
                    }
                }
                if (m.getState() == UPDATE_STATE.CREATE) {
                    if (!world.isVisible(m.getMappable())) {
                        world.removeCauseVisibility(m.getMappable().getId());
                        entityUpdate.add(new Update(UPDATE_STATE.HIDE,m.getMappable().clone()));
                    }
                }
            }
        }
    }

    // Handles new instances, updates of existing ones and removals. Yet another standard ABM
    public String getBoardUpdate(boolean noTimeoutLock) {
        if (noTimeoutLock) {
            while (entityUpdate.size() == 0) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        updateVisibilties();

        String ans = "{\"updates\":[";
        while (!entityUpdate.isEmpty()) {
            Update u = entityUpdate.poll();
            ans += u.toString();
            if (!entityUpdate.isEmpty()) {
                ans += ",";
            }
        }

        ans += "]}";
        entityUpdate.clear();
        return ans;
    }


    // Just informs opcodes of the controller status. Dont overthink it =)
    public String getControlUpdate(String name, boolean noTimeoutLock) {

        if (!pendingControlUpdate.containsKey(name)) {
            pendingControlUpdate.put(name, new LinkedList<>());
        }

        if (noTimeoutLock) {
            while (pendingControlUpdate.isEmpty()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        String ans = "{\"updates\":[";
        while (!pendingControlUpdate.get(name).isEmpty()) {
            String update = pendingControlUpdate.get(name).poll();
            ans += update;
            if (pendingControlUpdate.get(name).size() >= 1) {
                ans += ",";
            }
        }
        ans += "]}";

        pendingControlUpdate.get(name).clear();

        return ans;
    }


    public static String getStats(String name) {
        for (Player p : instance.players) {
            if (name != null && p != null && p.getName() != null && name.equals(p.getName())) {
                return p.toString();
            }
        }
        return "{\"error\":\"Name not found\"}";
    }

}
