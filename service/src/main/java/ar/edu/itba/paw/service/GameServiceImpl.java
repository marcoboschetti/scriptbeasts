package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.GameService;
import ar.edu.itba.paw.model.Characters.PlayerFactory;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService{

    @Override
    public void addPlayer(String name, int role) {
        MainGame.getInstance().addPlayer(name, role);
    }

    @Override
    public String getNewPlayer() {
        return MainGame.getInstance().getNewPlayer();
    }

    @Override
    public void informMove(String ign, int move) { MainGame.getInstance().informMove(ign,move);    }

    @Override
    public int requestInput(String ign) {
        return MainGame.getInstance().requestInput(ign);
    }

    @Override
    public String startGame(Integer width, Integer height, Integer player_cont) {
        String wholeGame = MainGame.getInstance().startGame(width,height, player_cont);
        System.out.println(wholeGame);
        return wholeGame;
    }

    @Override
    public String getUpdate() {
        return MainGame.getInstance().getBoardUpdate(true);
    }

    @Override
    public String getRoles() {
        return PlayerFactory.getRoles();
    }

    @Override
    public String getStats(String name) {
        return MainGame.getStats(name);
    }

    @Override
    public String getControlUpdate(String name) {
        return MainGame.getInstance().getControlUpdate(name,true);
    }

}
