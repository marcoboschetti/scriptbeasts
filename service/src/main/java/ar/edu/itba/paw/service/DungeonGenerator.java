package ar.edu.itba.paw.service;

import ar.edu.itba.paw.model.Characters.Player;
import ar.edu.itba.paw.model.*;
import ar.edu.itba.paw.model.NPC.CreatureGenerator;
import ar.edu.itba.paw.model.Mappable;
import ar.edu.itba.paw.model.WorldTiles.Path;
import ar.edu.itba.paw.model.WorldTiles.Stairs;
import ar.edu.itba.paw.model.WorldTiles.Wall;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;


public class DungeonGenerator {

    private static final int MINROOMSIZE = 2;
    private static final int MAXROOMSIZE = 4;
    private static int level = 0;


    public static void generateWorld(World worldWrapper, List<Player> players, List<Updatable> enemies) {

        level++;
        Mappable[][] world = worldWrapper.getMatrix();

        boolean done = false;
        while(!done){

            for(int i=0;i<world.length;i++){
                for (int j=0;j<world[0].length;j++){
                    if(world[i][j] != null){
                        world[i][j]= null;
                    }
                }
            }
            done = placeRooms(world,14+level*4, players, enemies, level);
        }

        for(int i=0;i<world.length;i++){
            for (int j=0;j<world[0].length;j++){
                if(world[i][j] == null){
                    world[i][j]= new Wall(i,j);
                }
            }
        }

        System.out.println("World "+world.length+" , "+world[0].length+" created");

        worldWrapper.calculateWarFog(players);
    }

    private static boolean placeRooms(Mappable[][] world, int MAXROOMS, List<Player> players, List<Updatable> enemies, int level) {
        List<Room> rooms = new LinkedList<>();

        boolean playersAdded = false;
        boolean stairsAdded = false;
        Point newCenter;

        for (int r = 0; r < MAXROOMS; r++) {
            int w = (int) (MINROOMSIZE + Math.random() * MAXROOMSIZE - MINROOMSIZE + 1);
            int h = (int) (MINROOMSIZE + Math.random() * MAXROOMSIZE - MINROOMSIZE + 1);
            int x = (int) (Math.random() * (world.length - w - 1) + 1);
            int y = (int) (Math.random() * (world[0].length - h - 1) + 1);

            Room newRoom = new Room(x, y, w, h);

            boolean failed = false;
            for (Room m : rooms) {
                if (newRoom.intersects(m)) {
                    failed = true;
                    break;
                }
            }
            if (!failed) {
                createRoom(newRoom, world);
                newCenter = newRoom.center;

                if (rooms.size() != 0) {
                    Point prevCenter = rooms.get(rooms.size() - 1).center;

                    if (Math.random() > 0.5) {
                        for (int i = Math.min(prevCenter.x, newCenter.x); i <= Math.max(prevCenter.x, newCenter.x); i++) {
                            world[i][prevCenter.y] = new Path(i, prevCenter.y);
                        }

                        for (int i = Math.min(prevCenter.y, newCenter.y); i <= Math.max(prevCenter.y, newCenter.y); i++) {
                            world[newCenter.x][i] = new Path(newCenter.x, i);
                        }

                    } else {
                        for (int i = Math.min(prevCenter.x, newCenter.x); i <= Math.max(prevCenter.x, newCenter.x); i++) {
                            world[i][newCenter.y] = new Path(i, newCenter.y);
                        }

                        for (int i = Math.min(prevCenter.y, newCenter.y); i <= Math.max(prevCenter.y, newCenter.y); i++) {
                            world[prevCenter.x][i] = new Path(prevCenter.x, i);
                        }
                    }
                }
            }
            boolean roomWithPlayers = false;
            if (!failed){
                rooms.add(newRoom);
                if(!playersAdded && newRoom.h*newRoom.w >= players.size()){
                    playersAdded = true;
                    roomWithPlayers = true;
                    int playerx = newRoom.x1;
                    int playery = newRoom.y1;
                    for(Player p: players){
                        p.setX(playerx);
                        p.setY(playery);
                        if(playerx+1 < newRoom.x2){
                            playerx++;
                        }else{
                            playerx = newRoom.x1;
                            playery++;
                        }
                    }
                }
                if(!roomWithPlayers){
                    List<Attackable> updatables = CreatureGenerator.getCreatures(level, newRoom.h * newRoom.w);
                    enemies.addAll(updatables);
                    int enemyx = newRoom.x1;
                    int enemyy = newRoom.y1;
                    for (Updatable u : updatables) {
                        u.setX(enemyx);
                        u.setY(enemyy);
                        if (enemyx + 1 < newRoom.x2) {
                            enemyx++;
                        } else {
                            enemyx = newRoom.x1;
                            enemyy++;
                        }
                    }
                    if(!stairsAdded && newRoom.h*newRoom.w >= players.size()){
                        stairsAdded = true;
                        int stairsx = newRoom.x1;
                        int stairsy = newRoom.y1;
                        for(Player p: players){
                            world[stairsx][stairsy] = new Stairs(stairsx,stairsy);
                            if(stairsx+1 < newRoom.x2){
                                stairsx++;
                            }else{
                                stairsx = newRoom.x1;
                                stairsy++;
                            }
                        }
                    }
                }
            }
        }
        return playersAdded;
    }

    private static void createRoom(Room newRoom, Mappable[][] world) {
        for(int i=newRoom.x1;i<newRoom.x2;i++) {
            for(int j=newRoom.y1;j<newRoom.y2;j++) {
                world[i][j] = new Path(i,j);
            }
        }
    }

    static class Room {
        public int x1;
        public int x2;
        public int y1;
        public int y2;

        public int w;
        public int h;

        public Point center;

        public Room(int x, int y, int w, int h) {
            super();

            x1 = x;
            x2 = x + w;
            y1 = y;
            y2 = y + h;
            this.w = w;
            this.h = h;
            center = new Point((int) Math.floor((x1 + x2) / 2),
                    (int) Math.floor((y1 + y2) / 2));
        }

        // return true if this room intersects provided room
        public boolean intersects(Room room) {
            return (x1 <= room.x2 && x2 >= room.x1 &&
                    y1 <= room.y2 && room.y2 >= room.y1);
        }
    }


}
