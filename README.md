### Yet another RPG Dungeons & Dragons-style web based game =D
 ###

### Failure:
 ###* The turns system makes the game really slow. Removes the speed wanted in an rpg multiplayer game.
* The next version wont be web-based, it needs to be installed and have a stronger social experience!
* The attacks and dungeon mechanics are good so far, the fog needs improvement.
* Items and monsters need to be explored deeper.
* The segmented roles make harder to progress. Consider making a skill-based dynamic role, so the player can choose the desired role while playing. The profile should be persisted.