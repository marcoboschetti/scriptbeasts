var grole_selected=0;
var count=1;

var roles;

var content="";

$( document ).ready(function() {
	getRequest('/getRoles',
		startSelection,
		nothing
		);
});

function startSelection(data){

	roles = JSON.parse(data).roles;

	for(var i=0;i<roles.length;i++){
		content+='&ensp;<div class="character_background_sprite"> <img src="resources/img/dirt_tile.png"> </div>';
	}

	resetMarks(roles[0].role_id);
	grole_selected = roles[0].role_id;

	$("#start_game").click(function() {
		if($("#player_name").val() != ""){		
			window.location.href = "player/add?role="+grole_selected+"&name="+$("#player_name").val();
		}else{
			alert("Perdón, pero necesitas elegir un nombre");
		}
	});

}


function resetMarks(selected){

	var role_selected;
	for(var i=0; i < roles.length; i++){
		if(roles[i].role_id == selected){
			role_selected = roles[i];
		}
	}

	if($("#character_tiles") == undefined){
		return;
	}

	var selection_content = "";

	for(var i=0;i<roles.length;i++){
		if(roles[i].role_id == selected){
			selection_content+='&ensp;<div class="character_selection_sprite" name="'+roles[i].role_id+'" > <img src="resources/img/Items/ball_red.png"> </div>';
		}else{
			selection_content+='&ensp;<div class="character_selection_sprite" name="'+roles[i].role_id+'" > <img src="resources/img/Items/ball_blue.png"> </div>';
		}
	}

	$("#character_selection_tiles").html(selection_content);

	updateCharacter(selected);

	$(".character_selection_sprite" ).click(function() {
		grole_selected = $(this).attr('name');
		resetMarks($(this).attr('name'));
		updateCharacter($(this).attr('name'));
	});

}

function updateCharacter(selected){
	var role_selected;
	for(var i=0; i < roles.length && role_selected==undefined; i++){
		if(roles[i].role_id == selected){
			role_selected = roles[i];
		}
	}

	$("#character_name").html('<h1 class="medieval light-gray">'+role_selected.name+'</h1>');
	$("#stat_inteligence_val").html(role_selected.stats[0]);
	$("#stat_force_val").html(role_selected.stats[1]);
	$("#stat_agility_val").html(role_selected.stats[2]);
	$("#stat_desxtrexity_val").html(role_selected.stats[3]);
	$("#stat_speed_val").html(role_selected.stats[4]);
	$("#stat_maxlife_val").html(role_selected.stats[5]);	

//	$("#desc").html(descriptions[selected][0]);
//	$("#hist").html(descriptions[selected][1]);
//	$("#skills").html(descriptions[selected][2]);

}

function character_walk(){
	if(roles == undefined){
		return;
	}

	var full_content = content;
	for(var i = 0; i < roles.length; i++){
		full_content +='&ensp;<div class="character_sprite" style="left:'+(30+(54*i))+'px;"> <img src="'+roles[i].image_prefix[2][count]+'"> </div>';
	}

	count= ((count+1)%4);

	$("#character_tiles").html(full_content);

}
