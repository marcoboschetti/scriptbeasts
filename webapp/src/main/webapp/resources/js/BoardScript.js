var renderer = PIXI.autoDetectRenderer(window.innerWidth - 16, window.innerHeight - 16, {backgroundColor: 0x000000});
document.body.appendChild(renderer.view);

// create the root of the scene graph
var stage = new PIXI.Container();
var entities;

var dungeon_scene;
var player_cont = 0;
var start_time = Date.now();
var fps = 90;

$(document).ready(function () {

    waitForPlayers();

});


function startDungeon(starting_data) {

    if(dungeon_scene != undefined){

        for(var i = 0; i < entities.length; i++){
            dungeon_scene.removeChild(entities[i]);
            entities[i] = undefined;
        }
        stage.removeChild(dungeon_scene);
    }

    dungeon_scene = new PIXI.Container();
    entities = {};

    stage.addChild(dungeon_scene);

    var wholeMap = JSON.parse(starting_data);
    var map = wholeMap.tiles;

    for (var i = 0; i < map.length; i++) {
        for (var j = 0; j < map[0].length; j++) {

            var tile_texture = PIXI.Texture.fromImage(map[i][j].sprite);
            var tile = new PIXI.Sprite(tile_texture);
            tile.anchor.x = 0;
            tile.anchor.y = 0;
            tile.width = 32;
            tile.height = 32;
            tile.position.x = 32 * i;
            tile.position.y = 32 * j;
            tile.visible = false;
            entities[map[i][j].id] = tile;

            dungeon_scene.addChild(tile);
        }
    }

    for (var i = 0; i < wholeMap.players.length; i++) {
        var current_player = wholeMap.players[i];
        var player_texture = PIXI.Texture.fromImage(current_player.image_prefix[2][1]);
        var player = new PIXI.Sprite(player_texture);
        player.anchor.x = 0;
        player.anchor.y = 0;
        player.position.x = current_player.x * 32;
        player.position.y = current_player.y * 32;

        player.textures = current_player.image_prefix;

        entities[current_player.id] = player;

        dungeon_scene.addChild(player);
    }

    for (var i = 0; i < wholeMap.enemies.length; i++) {
        var current_enemy = wholeMap.enemies[i];
        var enemy_texture = PIXI.Texture.fromImage(current_enemy.image_prefix[2][1]);
        var enemy = new PIXI.Sprite(enemy_texture);
        enemy.anchor.x = 0;
        enemy.anchor.y = 0;
        enemy.position.x = current_enemy.x * 32;
        enemy.position.y = current_enemy.y * 32;

        enemy.textures = current_enemy.image_prefix;

        enemy.visible = false;

        entities[current_enemy.id] = enemy;

        dungeon_scene.addChild(enemy);
    }

    renderer.render(stage);

    requestAnimationFrame(gameCycle);
}


function gameCycle(data) {

    renderer.render(stage);
    getRequest('/game/getUpdate',
        drawUpdate,
        nothing
    );
}


var update_index = -1;
var updates = [];

function drawUpdate(data) {

    if (data != undefined) {
        updates = JSON.parse(data).updates;
        update_index = -1;
    }

    update_index++;

    if (update_index < updates.length) {
        var update = updates[update_index];
        var entity = undefined;

        var segment = update.segment;
        var state = update.state;

        if (state == "UPDATE") {
            entity = entities[segment.id];
            if (segment.status != undefined) {
                drawStatus(entity, segment.status);
            } else {
                walkEntity(entity, segment.x * 32, segment.y * 32, segment.maxHealth, segment.health, segment.status);
            }
        } else if (state == "REMOVE") {
            entity = entities[segment.id];
            removeEntity(entity);
        } else if (state == "CREATE") {
            createEntity(segment);
        } else if (state == "SHOW") {
            entity = entities[segment.id];
            showEntity(entity);
        } else if (state == "HIDE") {
            entity = entities[segment.id];
            hideEntity(entity);
        } else if (state == undefined && update.currentMove != undefined) {
            updateCurrentMover(update.currentMove);
        } else if( update.NEW_WORLD != undefined) {
            startDungeon(JSON.stringify(update.NEW_WORLD));
        }else{
            drawUpdate();
        }
    } else {
        requestAnimationFrame(gameCycle);
    }
}

function removeEntity(entity) {
    if (entity.healthBar != undefined) {
        dungeon_scene.removeChild(entity.healthBar);
    }
    dungeon_scene.removeChild(entity);
    drawUpdate();
    return;
}

function showEntity(entity) {
    if (entity.healthBar != undefined) {
        entity.healthBar.visible = true;
    }
    entity.visible = true;
    drawUpdate();
    return;
}

function hideEntity(entity) {
    if (entity.healthBar != undefined) {
        entity.healthBar.visible = false;
    }
    entity.visible = false;
    drawUpdate();
    return;
}

var currentMover;

function updateCurrentMover(text) {
    if(currentMover == undefined) {
        var style = {font: "24px Arial", fill: "white"};
        currentMover = new PIXI.Text(text, style);
        currentMover.x = 0;
        currentMover.y = window.innerHeight - 16 - 32;
    }else{
        currentMover.text = text;
    }
    stage.removeChild(currentMover);
    stage.addChild(currentMover);
    
    drawUpdate();
    return;
}


function createEntity(entity) {
    var current_enemy = entity;
    var enemy_texture = PIXI.Texture.fromImage(current_enemy.image_prefix[2][1]);
    var enemy = new PIXI.Sprite(enemy_texture);
    enemy.anchor.x = 0;
    enemy.anchor.y = 0;
    enemy.position.x = current_enemy.x * 32;
    enemy.position.y = current_enemy.y * 32;
    enemy.textures = current_enemy.image_prefix;
    entities[current_enemy.id] = enemy;
    dungeon_scene.addChild(enemy);
    drawUpdate();
}

var TILE_SIDE = 32;
var HEALTH_SIDE = 32;

function drawHealth(entity, justPosition, maxHealth, health) {
    if (justPosition == true && entity.healthBar != undefined) {
        entity.healthBar.position.set(entity.position.x, entity.position.y - (HEALTH_SIDE / 2));
        return;
    }
    if (maxHealth == undefined || health == undefined) {
        return;
    }
    var healthBar = entity.healthBar;
    if (healthBar == undefined) {
        //Create the health bar
        healthBar = new PIXI.Container();
        healthBar.position.set(entity.position.x, entity.position.y - (HEALTH_SIDE / 2));
        dungeon_scene.addChild(healthBar);
        entity.healthBar = healthBar;

        //Create the black background rectangle
        var innerBar = new PIXI.Graphics();
        innerBar.beginFill(0x000000);
        innerBar.drawRect(0, 0, TILE_SIDE, 8);
        innerBar.endFill();
        healthBar.addChild(innerBar);

        //Create the front red rectangle
        var outerBar = new PIXI.Graphics();
        outerBar.beginFill(0xFF3300);
        outerBar.drawRect(0, 0, HEALTH_SIDE * (health / maxHealth), 8);
        outerBar.endFill();
        healthBar.addChild(outerBar);

        healthBar.outer = outerBar;

    } else {
        healthBar.position.set(entity.position.x, entity.position.y - (HEALTH_SIDE / 2));
        var outerBar = healthBar.outer;
        outerBar.beginFill(0x000000);
        outerBar.drawRect(0, 0, HEALTH_SIDE, 8);
        outerBar.endFill();
        outerBar.beginFill(0xFF3300);
        outerBar.drawRect(0, 0, HEALTH_SIDE * (health / maxHealth), 8);
        outerBar.endFill();
    }

}


function drawStatus(entity, status) {
    if (status == "WHITE") {
        entity.visible = true;
        entity.alpha = 1;
    } else if (status == "GRAY") {
        entity.visible = true;
        entity.alpha = 0.50;
    }
    else {
        entity.visible = false;
    }
    drawUpdate();
    return;
}

function walkEntity(entity, newx, newy, maxHealth, health) {

    drawHealth(entity, false, maxHealth, health);

    entity.walking_sprite = 0;
    entity.endx = newx;
    entity.endy = newy;
    entity.walking_ended = false;
    if (entity.x == newx) {
        if (entity.y > newy) {
            entity.walking_way = 0;
            animateWalk(entity);
            entity.texture = PIXI.Texture.fromImage(entity.textures[0][0]);
        } else {
            entity.walking_way = 2;
            animateWalk(entity);
            entity.texture = PIXI.Texture.fromImage(entity.textures[2][0]);
        }
    } else {
        if (entity.x > newx) {
            entity.walking_way = 3;
            animateWalk(entity);
            entity.texture = PIXI.Texture.fromImage(entity.textures[3][0]);
        } else {
            entity.walking_way = 1;
            animateWalk(entity);
            entity.texture = PIXI.Texture.fromImage(entity.textures[1][0]);
        }
    }

}

var STEP_SIZE = 2;

function animateWalk(entity) {

    var new_sprite = -1;
    if (entity.walking_way == 1 || entity.walking_way == 3) {
        new_sprite = Math.floor((entity.x % TILE_SIDE) / (TILE_SIDE / 4)) % 4;
    } else {
        new_sprite = Math.floor((entity.y % TILE_SIDE) / (TILE_SIDE / 4)) % 4;
    }

    if (entity.walking_ended) {
        entity.x = entity.endx;
        entity.y = entity.endy;

        drawUpdate();
        return;
    }
    if (entity.walking_way == 0) {
        entity.y -= STEP_SIZE;
        if (entity.walking_sprite != new_sprite) {
            entity.walking_sprite = new_sprite;
            entity.texture = PIXI.Texture.fromImage(entity.textures[0][new_sprite]);
        }
        if (entity.y <= entity.endy) {
            entity.walking_ended = true;
        }
        drawHealth(entity, true);
    } else if (entity.walking_way == 1) {
        entity.x += STEP_SIZE;
        if (entity.walking_sprite != new_sprite) {
            entity.walking_sprite = new_sprite;
            entity.texture = PIXI.Texture.fromImage(entity.textures[1][new_sprite]);
        }
        if (entity.x >= entity.endx) {
            entity.walking_ended = true;
        }
        drawHealth(entity, true);
    } else if (entity.walking_way == 2) {
        entity.y += STEP_SIZE;
        if (entity.walking_sprite != new_sprite) {
            entity.walking_sprite = new_sprite;
            entity.texture = PIXI.Texture.fromImage(entity.textures[2][new_sprite]);
        }
        if (entity.y >= entity.endy) {
            entity.walking_ended = true;
        }
        drawHealth(entity, true);
    } else {
        entity.x -= STEP_SIZE;
        if (entity.walking_sprite != new_sprite) {
            entity.walking_sprite = new_sprite;
            entity.texture = PIXI.Texture.fromImage(entity.textures[3][new_sprite]);
        }
        if (entity.x <= entity.endx) {
            entity.walking_ended = true;
        }
        drawHealth(entity, true);
    }
    renderer.render(stage);
    requestAnimationFrame(function () {
        animateWalk(entity);
    });
}
