var player_ready = 0;
var selection_players=[];
var waiting_scene = new PIXI.Container();   

function waitForPlayers(){
	stage.addChild(waiting_scene);

	var logo_texture = PIXI.Texture.fromImage('resources/img/Text/logo.png');
	var logo = new PIXI.Sprite(logo_texture);
	logo.anchor.x = 0.5;
	logo.anchor.y = 0.5;
	logo.position.x = (innerWidth-16)/2;
	logo.position.y = (innerHeight-16)/4;
	waiting_scene.addChild(logo);

	var waiting_texture = PIXI.Texture.fromImage('resources/img/Text/waiting_players.png');
	var waiting = new PIXI.Sprite(waiting_texture);
	waiting.anchor.x = 0.5;
	waiting.anchor.y = 0.5;
	waiting.position.x = (innerWidth-16)/2;
	waiting.position.y = (innerHeight-16)/3;
	waiting_scene.addChild(waiting);

	getNewPlayer();
}

function getNewPlayer() {

	getRequest('/player/getNewPlayer',
		drawIfPlayer,
		nothing
		);

	for(var i = 0; i < player_cont; i++){
		getRequest('/input/requestInput/'+selection_players[i].name,
			parseInput,
			nothing
			);
	}

	if(Date.now() - start_time > fps){
		redrawWaitingPlayers();
		renderer.render(stage);
		start_time = Date.now();
	}

	if(player_cont == 0 || player_cont != player_ready){
		requestAnimationFrame(getNewPlayer);
	}else{
		waiting_scene.visible = false;
		getRequest('/game/start?width='+(window.innerWidth-16)+'&height='+(innerHeight-16)+'&player_cont='+player_cont,
		startDungeon,
		nothing
		);
	}
	
}

function parseInput(data){
	var input = JSON.parse(data);
	if(input.move == "0"){

		for(var i = 0; i < player_cont; i++){
			var storage_player = selection_players[i];
			if(storage_player.name == input.name && storage_player.ready == 0){
				
				var red_ball_texture = PIXI.Texture.fromImage('resources/img/Items/ball_gold.png');
				var red_ball = new PIXI.Sprite(red_ball_texture);
				red_ball.anchor.x = 0.5;
				red_ball.anchor.y = 0.5;
				red_ball.position.x = (window.innerWidth-16)/2 - 400 + 128 * i;
				red_ball.position.y = (window.innerHeight-16)/2 + 60 ;
				storage_player.ready = 1;
				player_ready += 1;
				waiting_scene.addChild(red_ball);
			}
		}
	}
}


function redrawWaitingPlayers(){
	for(var i=0; i < player_cont; i++){

		var new_texture = PIXI.Texture.fromImage(selection_players[i].image_prefix[2][selection_players[i].texture_count]);

		selection_players[i].player.texture = new_texture;
		selection_players[i].texture_count = (selection_players[i].texture_count + 1)%4;
	}
}


function drawIfPlayer(data){
	if(data.trim()!=""){
		var player_data = JSON.parse(data);

		var text = new PIXI.Text(player_data.name, {font:"25px Arial", fill:"white"});
		var player_texture = PIXI.Texture.fromImage(player_data.image_prefix[2][0]);
		var player = new PIXI.Sprite(player_texture);
		player.anchor.x = 0.5;
		player.anchor.y = 0.5;
		player.position.x = (window.innerWidth-16)/2 - 400 + 128 * player_cont;
		player.position.y = (window.innerHeight-16)/2 + 30 ;
		text.anchor.x = 0.5;
		text.anchor.y = 0.5;
		text.position.x = (window.innerWidth-16)/2 - 400 + 128 * player_cont;
		text.position.y = (window.innerHeight-16)/2;

		waiting_scene.addChild(text);
		waiting_scene.addChild(player);

		var storage_player={};
		storage_player.player = player;
		storage_player.text = text;
		storage_player.name = player_data.name;
		storage_player.image_prefix = player_data.image_prefix;
		storage_player.texture_count = 2;
		storage_player.ready=0;
		selection_players[player_cont] = storage_player;

		player_cont++;
	}
}