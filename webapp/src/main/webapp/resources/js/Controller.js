var full_character;
var name;

$( document ).ready(function() {

    name = getUrlVars()["name"];

    getRequest('/player/stats?name='+name,
        drawStats,
        nothing
        );

    getControlUpdate();

    $("#ArrowU").click(function() {
        getRequest('/input/'+name+'/1',
            nothing,
            nothing
            );
    });

    $("#ArrowR").click(function() {
        getRequest('/input/'+name+'/2',
            nothing,
            nothing
            );
    });

    $("#ArrowD").click(function() {
        getRequest('/input/'+name+'/3',
            nothing,
            nothing
            );
    });

    $("#ArrowL").click(function() {
        getRequest('/input/'+name+'/4',
            nothing,
            nothing
            );
    });

    $("#ArrowUR").click(function() {
        getRequest('/input/'+name+'/5',
            nothing,
            nothing
        );
    });

    $("#ArrowDR").click(function() {
        getRequest('/input/'+name+'/6',
            nothing,
            nothing
        );
    });

    $("#ArrowDL").click(function() {
        getRequest('/input/'+name+'/7',
            nothing,
            nothing
        );
    });

    $("#ArrowUL").click(function() {
        getRequest('/input/'+name+'/8',
            nothing,
            nothing
        );
    });

    $("#Circle").click(function() {
        getRequest('/input/'+name+'/0',
            nothing,
            nothing
            );
    });

    $("#ControllWeapon").click(function() {
        getRequest('/input/'+name+'/10',
            nothing,
            nothing
        );
    });

});

function drawStats(data){

    var character = JSON.parse(data);

    full_character = character;
    
    drawInventory(character.inventory);
    $("#character_name").html('<h1 class="medieval light-gray">'+character.name+'</h1>');
    $("#stat_inteligence_val").html(character.stats[0]);
    $("#stat_force_val").html(character.stats[1]);
    $("#stat_agility_val").html(character.stats[2]);
    $("#stat_desxtrexity_val").html(character.stats[3]);
    $("#stat_speed_val").html(character.stats[4]);
    $("#stat_maxlife_val").html(character.stats[5]);    
    $("#stat_curlife_val").html(character.stats[5]);    

}


function drawInventory(inventory){

    var full_element='<div class="row">';
    for(var i = 0; i < inventory.length; i++){
        full_element+='<div class="col-sm-2 button_up"> <img src="'+inventory[i].image_prefix[0][0]+ '"> </div>';
        if(i %6 == 0 && i>0){
            full_element+='</div><div class="row">';
        }

        if(inventory[i].id == full_character.equiped_weapon_id){
            equipItem(inventory[i]);
        }

    }
    full_element += "</div>";
    $("#inventory_container").html(full_element);

}

function equipItem(item){
    $("#ControllWeapon").html('<img src="'+item.image_prefix[0][0]+ '">');
}

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getControlUpdate(){
    getRequest('/player/getControlUpdate?name='+name,
        parseControlUpdate,
        nothing
    );
}

function parseControlUpdate(data){

    var new_updates = JSON.parse(data).updates;
    for(var i = 0; i < new_updates.length ; i++){
      var update = new_updates[i];
        if(update.type != undefined){
            if(update.type == "attack"){
                updateAttack(update);
            }else if (update.type == "stats"){
                updateStats(update);
            }
        }
    }
    getControlUpdate();
}


function updateStats(update){
    $("#stat_"+update.field+"_val").html(update.value);
}

function updateAttack(update){
    if(update.attackMode != undefined){
        if(update.attackMode == "true"){
            $('.button_attack_hidden').addClass('button_attack_visible').removeClass('button_attack_hidden');
        }else{
            $('.button_attack_visible').addClass('button_attack_hidden').removeClass('button_attack_visible');
        }
    }
}