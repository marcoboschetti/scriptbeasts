<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  
  <title>ScriptBeasts</title>
  
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
  <script type="text/javascript" src="resources/js/CharacterSelection.js"></script>
  <script src="../resources/js/Library.js"></script>

  <link rel="stylesheet" href="resources/css/controller.css" type="text/css" />
  <link rel="stylesheet" href="resources/css/scrollablePanel.css" type="text/css" />

</head>

<body onload="setInterval('character_walk()', 150);">

  <div id="character_pick">
    <h1 class="medieval light-gray"> Selección de Personaje </h1>

    <div class="row">
      <div class="col-sm-3"> </div>
      <div class="col-sm-6">
        <div id="character_tiles">
        </div>
        <div id="character_selection_tiles">
        </div>
      </div>
      <div class="col-sm-3"> </div>
    </div>
    <div class="row">
      <div class="col-sm-1"> </div>

      <div class="col-sm-10">
        <div class="form-group">
          <label for="usr" class="light-gray">Nombre:</label>
          <input type="text" class="form-control" id="player_name">
        </div>
        <br>
        <div id="character_name"></div>
        <div id="character_stats"> 
          <div class="row">
           <div class="col-sm-1"></div>
           <div class="col-sm-5 stat_inteligence">Inteligencia: <span id="stat_inteligence_val"> </span></div> 
           <div class="col-sm-5 stat_force">Fuerza:  <span id="stat_force_val"> </span></div>
           <div class="col-sm-1"></div> 
         </div> 
         <div class="row">
           <div class="col-sm-1"></div>
           <div class="col-sm-5 stat_agility">Agilidad:  <span id="stat_agility_val"> </span></div> 
           <div class="col-sm-5 stat_desxtrexity">Destreza: <span id="stat_desxtrexity_val"> </span></div>
           <div class="col-sm-1"></div> 
         </div>
          <div class="row">
           <div class="col-sm-1"></div>
           <div class="col-sm-5 stat_speed">Velocidad:  <span id="stat_speed_val"> </span></div> 
           <div class="col-sm-5 stat_maxlife">Vida Máx.: <span id="stat_maxlife_val"> </span></div>
           <div class="col-sm-1"></div> 
         </div>
       </div>
     </div>
     <!--
     <div class="col-sm-5">
       <div id="character_description">

        <div id="character_description_container" class="container">
          <div class="row">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Rol</h3>
                <span class="pull-right">
                  <ul class="nav panel-tabs">
                    <li class="active"><a href="#desc" data-toggle="tab">Descripción</a></li>
                    <li><a href="#hist" data-toggle="tab">Historia</a></li>
                    <li><a href="#skills" data-toggle="tab">Habilidades</a></li>
                  </ul>
                </span>
              </div>            
              <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="desc">

                  </div>
                  <div class="tab-pane" id="hist">

                  </div>
                  <div class="tab-pane" id="skills">

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    -->
    <div class="col-sm-1"> </div>

  </div>

  <div class="text-center">
    <button type="button" id="start_game" class="btn btn-success">Entrar al Juego</button>
  </div>
</div>

</body>

</html>