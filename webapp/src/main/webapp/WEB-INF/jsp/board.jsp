<html class="no-js"> <!--<![endif]-->
<head>
  <title>Script Beasts</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <meta name="apple-mobile-web-app-capable" content="yes">

</head>
<body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
            <![endif]-->

            <!-- Add your site or application content here -->


            <link href='http://fonts.googleapis.com/css?family=Orbitron:900' rel='stylesheet' type='text/css'>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
            <script src="../resources/js/pixi.min.js"></script>

            <script src="../resources/js/Library.js"></script>
            <script src="../resources/js/WaitForPlayers.js"></script>
            <script src="../resources/js/BoardScript.js"></script>

            <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
            <script>
              var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
              (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
                g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g,s)}(document,'script'));



            /**
             * Provides bind in a cross browser way.
             */
             if (typeof Function.prototype.bind != 'function') {
              Function.prototype.bind = (function () {
                var slice = Array.prototype.slice;
                return function (thisArg) {
                  var target = this, boundArgs = slice.call(arguments, 1);

                  if (typeof target != 'function') throw new TypeError();

                  function bound() {
                    var args = boundArgs.concat(slice.call(arguments));
                    target.apply(this instanceof bound ? this : thisArg, args);
                  }

                  bound.prototype = (function F(proto) {
                    proto && (F.prototype = proto);
                    if (!(this instanceof F)) return new F;          
                  })(target.prototype);

                  return bound;
                };
              })();
            }
          </script>

        </body>
        </html>
