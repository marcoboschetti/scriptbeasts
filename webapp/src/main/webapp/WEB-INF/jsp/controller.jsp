<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>ScriptBeasts Control</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=1.0, user-scalable=no" />

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="../resources/css/controller.css" type="text/css" />
	<script src="../resources/js/Library.js"></script>
	<script src="../resources/js/Controller.js"></script>

</head>

<body>

	<div id="controller" >
		<ul class="nav nav-pills">
			<li class="active"><a data-toggle="pill" href="#home">Control</a></li>
			<li><a data-toggle="pill" href="#menu1">Inventario</a></li>
			<li><a data-toggle="pill" href="#menu2">Información</a></li>
		</ul>

		<div class="tab-content">
			<div id="home" class="tab-pane fade in active">

				<div class="container">
					<h3 class="control_tab">Control</h3>

					<div class="row">
						<div class="col-sm-5">

							<div class="row">
								<div class="col-sm-4 button_attack_hidden" id="ArrowUL">
									<img src="../resources/img/Arrows/Arrow_UL.png">
								</div>
								<div class="col-sm-4 button_up" id="ArrowU">
									<img src="../resources/img/Arrows/Arrow_U.png">
								</div>
								<div class="col-sm-4 button_attack_hidden" id="ArrowUR">
									<img src="../resources/img/Arrows/Arrow_UR.png">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4 button_left" id="ArrowL" >
									<img src="../resources/img/Arrows/Arrow_L.png">
								</div>
								<div class="col-sm-4 button_null" id="Circle">
									<img src="../resources/img/Arrows/Circle.png">
								</div>
								<div class="col-sm-4 button_right" id="ArrowR">
									<img src="../resources/img/Arrows/Arrow_R.png">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4 button_attack_hidden" id="ArrowDL">
									<img src="../resources/img/Arrows/Arrow_DL.png">
								</div>
								<div class="col-sm-4 button_down" id="ArrowD">
									<img src="../resources/img/Arrows/Arrow_D.png">
								</div>
								<div class="col-sm-4 button_attack_hidden" id="ArrowDR">
									<img src="../resources/img/Arrows/Arrow_DR.png">
								</div>
							</div>
                            <div class="row">
                                <div class="col-sm-1 button_null">
                                </div>
                                <div class="col-sm-4 button_weapon" id="ControllWeapon">
                                </div>
                                <div class="col-sm-2 button_null">
                                </div>
                                <div class="col-sm-4 button_null">
                                </div>
                                <div class="col-sm-1 button_null">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1 button_null">
                                </div>
                                <div class="col-sm-4 button_weapon" id="ControlWeaponText">
                                </div>
                                <div class="col-sm-2 button_null">
                                </div>
                                <div class="col-sm-4 button_null">
                                </div>
                                <div class="col-sm-1 button_null">
                                </div>
                            </div>
						</div>
						<div class="col-sm-7">


							<div class="stat_name"></div>
							<br>
							<div class="current_life">Vida actual: <span id="stat_curlife_val"></span> </div>
							<br>

							<div id="character_stats"> 
								<div class="row">
									<div class="col-sm-5 stat_inteligence">Inteligencia: <span id="stat_inteligence_val"> </span></div> 
									<div class="col-sm-5 stat_force">Fuerza:  <span id="stat_force_val"> </span></div>
								</div> 
								<div class="row">
									<div class="col-sm-5 stat_agility">Agilidad:  <span id="stat_agility_val"> </span></div> 
									<div class="col-sm-5 stat_desxtrexity">Destreza: <span id="stat_desxtrexity_val"> </span></div>
								</div>
								<div class="row">
									<div class="col-sm-5 stat_speed">Velocidad:  <span id="stat_speed_val"> </span></div> 
									<div class="col-sm-5 stat_maxlife">Vida Máx.: <span id="stat_maxlife_val"> </span></div>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<div id="menu1" class="tab-pane fade">
				<div class="container">
					<h3 class="control_tab">Inventario</h3>

					<div id="inventory_container">
					</div>

				</div>
			</div>
			<div id="menu2" class="tab-pane fade">
				<h3>Información</h3>
				<div id="page-wrap">

					<h2>jQuery/PHP Chat</h2>

					<p id="name-area"></p>

					<div id="chat-wrap"><div id="chat-area"></div></div>

				</div>
			</div>
		</div>
	</div>


</body>

</html>