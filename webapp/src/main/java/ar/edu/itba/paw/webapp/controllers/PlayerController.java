package ar.edu.itba.paw.webapp.controllers;


import ar.edu.itba.paw.interfaces.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PlayerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);


    @Autowired
    GameService gameService;

    // Character selection Screen
    @RequestMapping(value = {"/getRoles"})
    public ModelAndView getRoles() {

        ModelAndView mav = new ModelAndView("jsonWrapper");
        String ans= gameService.getRoles();

        mav.addObject("json",ans);
        return mav;
    }

    // Character selection Screen
    @RequestMapping(value = {"/player/add"})
    public
    @ResponseBody
    ModelAndView addPlayer(@RequestParam(required = false) final Integer role,
                              @RequestParam(required = false) final String name) {
        gameService.addPlayer(name, role);

        ModelAndView mav = new ModelAndView("controller");
        return mav;
    }


    @RequestMapping(value = {"/input/{ign}/{move}"})
    public ModelAndView getInput(@PathVariable String ign, @PathVariable int move) {
        gameService.informMove(ign, move);

        ModelAndView mav = new ModelAndView("jsonWrapper");
        return mav;
    }

    // Just used to know id a player is ready to play
    @RequestMapping(value = {"/input/requestInput/{ign}"})
    public ModelAndView getInput(@PathVariable String ign) {
        int move = gameService.requestInput(ign);

        ModelAndView mav = new ModelAndView("jsonWrapper");
        String ans= "{\"name\":\""+ign+"\",\"move\":\""+move+"\"}";
        mav.addObject("json",ans);
        return mav;
    }

    // Character control Screen
    @RequestMapping(value = {"/player/stats"})
    public
    @ResponseBody
    ModelAndView getStats(@RequestParam(required = false) final String name) {
        String ans = gameService.getStats(name);

        ModelAndView mav = new ModelAndView("jsonWrapper");
        mav.addObject("json",ans);
        System.out.println(ans);
        return mav;
    }

    @RequestMapping(value = {"/player/getControlUpdate"})
    public
    @ResponseBody
    ModelAndView getControlUpdate(@RequestParam(required = false) final String name) {
        String ans = gameService.getControlUpdate(name);

        ModelAndView mav = new ModelAndView("jsonWrapper");
        mav.addObject("json",ans);
        return mav;
    }

}