package ar.edu.itba.paw.webapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class MainController {
//    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

// Character selection Screen
    @RequestMapping(value = {"/index.html"})
    public ModelAndView main() {

        ModelAndView mav = new ModelAndView("index");
        return mav;
    }

// Redirect to Character selection Screen
    @RequestMapping(value = {"/"})
    public ModelAndView defaultMapping() {
        return new ModelAndView("redirect:/index.html");
    }

    //Not sure what for
    @RequestMapping(value = {"/favicon.ico "})
    public ModelAndView favicon() {
        return new ModelAndView("jsonWrapper");
    }
}
