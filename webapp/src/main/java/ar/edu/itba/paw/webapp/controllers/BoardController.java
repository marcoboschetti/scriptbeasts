package ar.edu.itba.paw.webapp.controllers;


import ar.edu.itba.paw.interfaces.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BoardController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Autowired
    GameService gameService;

    @RequestMapping(value = {"/board.html"})
    public ModelAndView board() {
        gameService.getNewPlayer();

        ModelAndView mav = new ModelAndView("board");
        return mav;
    }

    // Character selection Screen
    @RequestMapping(value = {"/player/getNewPlayer"})
    public ModelAndView getNewPlayer() {
        String newPlayer = gameService.getNewPlayer();
        ModelAndView mav = new ModelAndView("jsonWrapper");
        mav.addObject("json",newPlayer);
        return mav;
    }

    @RequestMapping(value = {"/game/start"})
    public
    @ResponseBody
    ModelAndView startGame(@RequestParam(required = false) final Integer width,
                           @RequestParam(required = false) final Integer height,
                           @RequestParam(required = false) final Integer player_cont) {
        String game = gameService.startGame(width,height,player_cont);
        ModelAndView mav = new ModelAndView("jsonWrapper");
        mav.addObject("json",game);
        return mav;
    }

    // Main Game cycle, drawing in frontend's half =D
    @RequestMapping(value = {"/game/getUpdate"})
    public ModelAndView getUpdate() {
        String update = gameService.getUpdate();
        ModelAndView mav = new ModelAndView("jsonWrapper");
        mav.addObject("json",update);
        System.out.println("ToBoard: "+update);
        return mav;
    }


}