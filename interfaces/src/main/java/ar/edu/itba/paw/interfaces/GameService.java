package ar.edu.itba.paw.interfaces;

public interface GameService {

    void addPlayer(String name, int role);

    String getNewPlayer();

    void informMove(String ign, int move);

    int requestInput(String ign);

    String startGame(Integer width, Integer height, Integer player_cont);

    String getUpdate();

    String getRoles();

    String getStats(String name);

    String getControlUpdate(String name);
}
